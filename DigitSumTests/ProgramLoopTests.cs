﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DigitSum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace DigitSum.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void FindMaxDigitSumTest_ValidData_ValidResult()
        {
            //arrange
            var mockprogramLoop = new Mock<ProgramLoop>
            {
                CallBase = true
            };
            mockprogramLoop.Setup(x => x.SumDigit(2)).Returns(2);
            mockprogramLoop.Setup(x => x.SumDigit(4)).Returns(4);
            mockprogramLoop.Setup(x => x.SumDigit(3)).Returns(3);
            mockprogramLoop.Setup(x => x.SumDigit(20)).Returns(2);
            mockprogramLoop.Setup(x => x.SumDigit(21)).Returns(3);
            mockprogramLoop.Setup(x => x.SumDigit(19)).Returns(10);
            var numbers1 = new int[] { 2, 4, 3 };
            var numbers2 = new int[] { 20, 21, 19 };
            //act
            var result1 = mockprogramLoop.Object.FindMaxDigitSum(numbers1);
            var result2 = mockprogramLoop.Object.FindMaxDigitSum(numbers2);
            //assert
            Assert.AreEqual(1, result1);
            Assert.AreEqual(2, result2);
        }

        [TestMethod()]
        public void SumDigit_ValidData_ValidResult()
        {
            var programLoop = new ProgramLoop();

            var result1 = programLoop.SumDigit(0);
            var result2 = programLoop.SumDigit(999);
            var result3 = programLoop.SumDigit(34);
            var result4 = programLoop.SumDigit(1);
            var result5 = programLoop.SumDigit(12);
            var result6 = programLoop.SumDigit(10000001);

            Assert.AreEqual(0, result1);
            Assert.AreEqual(27, result2);
            Assert.AreEqual(7, result3);
            Assert.AreEqual(1, result4);
            Assert.AreEqual(3, result5);
            Assert.AreEqual(2, result6);
        }
    }
}