﻿using System;
using System.Linq;

namespace DigitSum
{
    public class ProgramLoop
    {
        public void Run()
        {
            var numbers = new int[Int32.Parse(Console.ReadLine())];
            numbers = Console.ReadLine().Split(' ').Select(x => Int32.Parse(x)).ToArray();
            Console.WriteLine(FindMaxDigitSum(numbers));
            Console.ReadKey();
        }

        public int FindMaxDigitSum(int[] numbers)
        {
            var sumOfDigits = 0;
            var number = 0;
            var numberIndex = 0;

            for(var i = 0; i < numbers.Length; i++)
            {
                var sum = SumDigit(numbers[i]);
                if(sum > sumOfDigits || sum == sumOfDigits && numbers[i] > number)
                {
                    sumOfDigits = sum;
                    number = numbers[i];
                    numberIndex = i;
                }
            }
            return numberIndex;
        }

        public virtual int SumDigit(int number)
        {
            int sum = 0;
            while (number != 0)
            {
                sum += number % 10;
                number /= 10;
            }
            return sum;
        }
    }
}
